#!/usr/bin/env python3
# coding: UTF-8

"""
Sheep for Tkinter/python

License: WTFPL
"""
try:
    from tkinter import *
except:
    from Tkinter import *
import random
from time import *


class ASheep():
    def __init__(self):
        self.is_running = False
        self.x = 0
        self.y = 0
        self.progress_on_jump = 0
        self.x_on_jump = 0
        self.image = None
        self.stretch_leg = False


class SheepFrame(Frame):

    FPS = 10

    sleep_time = FPS*10

    TITLE = "sheep"
    DEFAULT_WIDTH = 120
    DEFAULT_HEIGHT = 120

    MAX_SHEEP_NUMBER = 100

    COUNTER_POSITION_X = 4
    COUNTER_POSITION_Y = 4

    X_MOVING_DIST = 5
    Y_MOVING_DIST = 3

    TOP_ON_JUMP = 5

    GROUND_HEIGHT_RATIO = 0.9

    FENCE_IMAGE_NAME = "fence.gif"
    SHEEP00_IMAGE_NAME = "sheep00.gif"
    SHEEP01_IMAGE_NAME = "sheep01.gif"

    SKY_COLOR = "#9696FF"
    GROUND_COLOR = "#64FF64"

    sheep_flock = []

    sheep_count = 0

    ground_height = 0

    gate_is_widely_open = False

    running = False

    fence_bitmap = None

    sheep_bitmap = []

    fence_image = None

    sheep_image = []

    counter_format = "羊が%d匹"
    
    c=None

    id_counter=None

    BUTTON_1="<Button-1>"
    
    BUTTON_RELEASE_1="<ButtonRelease-1>"

    def __init__(self, parent):
        Frame.__init__(self, parent)

        self.parent = parent
        self.parent.title(self.TITLE)
        self.c = Canvas(self, width=self.DEFAULT_WIDTH, height=self.DEFAULT_HEIGHT)
        self.c.pack()
        self.pack()

        # prepare source of fence image
        self.fence_image=PhotoImage(file=self.FENCE_IMAGE_NAME)

        # define ground height from fence height
        self.ground_height=int(self.fence_image.height() * self.GROUND_HEIGHT_RATIO)
        # draw the sky as rectangle on canvas
        self.c.create_rectangle(0, 0, self.DEFAULT_WIDTH, self.DEFAULT_HEIGHT, fill=self.SKY_COLOR, outline=self.SKY_COLOR)
        # draw the ground as rectangle on canvas
        self.c.create_rectangle(0, self.DEFAULT_HEIGHT-int(self.fence_image.height() * self.GROUND_HEIGHT_RATIO), self.DEFAULT_WIDTH, self.DEFAULT_HEIGHT, fill=self.GROUND_COLOR, outline=self.GROUND_COLOR)

        # draw the indicator as text on canvas
        self.id_counter = self.c.create_text(self.COUNTER_POSITION_X, self.COUNTER_POSITION_Y, text=(self.counter_format % self.sheep_count), anchor='nw', font=('arial', 10, 'bold'))

        # create fence image
        self.c.create_image((self.DEFAULT_WIDTH-self.fence_image.width())/2, self.DEFAULT_HEIGHT-self.fence_image.height(), image=self.fence_image, anchor='nw')

        # prepare source of sheep images
        self.sheep_image.append(PhotoImage(file=self.SHEEP00_IMAGE_NAME))
        self.sheep_image.append(PhotoImage(file=self.SHEEP01_IMAGE_NAME))

        # initialize sheep_flock
        for i in range(0, self.MAX_SHEEP_NUMBER):
            self.sheep_flock.append(ASheep())

        # regist callback func for key inputs
        self.c.bind(self.BUTTON_1, self.callback_button_1)
        self.c.bind(self.BUTTON_RELEASE_1, self.callback_button_release_1)

        # Add first sheep
        self.send_out_sheep(self.sheep_flock[0])

    def start(self):
        self.run()

    def update(self):
        # Send out a sheep
        if self.gate_is_widely_open:
            for asheep in self.sheep_flock:
                if not asheep.is_running:
                    self.send_out_sheep(asheep)
                    break

        # Update status for each sheep
        for asheep in self.sheep_flock:
            if not asheep.is_running:
                continue

            # Run
            asheep.x -= self.X_MOVING_DIST

            # start to jump when sheep reach to jump point
            if not asheep.progress_on_jump and asheep.x_on_jump <= asheep.x and asheep.x < asheep.x_on_jump + self.X_MOVING_DIST:
                asheep.progress_on_jump += 1

            # behavior during jump
            if asheep.progress_on_jump > 0:
                if asheep.progress_on_jump < self.TOP_ON_JUMP:
                    asheep.y -= self.Y_MOVING_DIST
                    asheep.progress_on_jump += 1
                elif asheep.progress_on_jump < self.TOP_ON_JUMP * 2:
                    asheep.y += self.Y_MOVING_DIST
                    asheep.progress_on_jump += 1
                else:
                    asheep.progress_on_jump = 0

            # count up
            if asheep.progress_on_jump == self.TOP_ON_JUMP:
                self.sheep_count += 1

            # go away and send out a sheep if there is no sheep on run
            if asheep.x < -1 * self.sheep_image[0].width():
                some_sheep_is_running = False
                self.remove_sheep(asheep)
                for asheep in self.sheep_flock:
                    if asheep.is_running:
                        some_sheep_is_running = True
                        break
                if not some_sheep_is_running:
                    self.send_out_sheep(asheep)

    def remove_sheep(self, asheep):
        asheep.__init__()

    def send_out_sheep(self, asheep):
        asheep.is_running = True
        asheep.x = self.DEFAULT_WIDTH + self.sheep_image[0].width()
        asheep.y = self.DEFAULT_HEIGHT - self.ground_height + random.randrange(self.ground_height - self.sheep_image[0].height())
        asheep.x_on_jump = self.calc_jump_x(asheep.y)

    def calc_jump_x(self, y):
        fw = self.fence_image.width()
        fh = self.fence_image.height()
        return -1 * (y - self.DEFAULT_HEIGHT) * fw / fh + (self.DEFAULT_WIDTH - fw) / 2

    def run(self):
        self.update()
        self.draw()
        self.parent.after(self.sleep_time, self.run)

    def draw(self):
        # Draw the sheep
        for asheep in self.sheep_flock:
            if asheep.is_running:
                if asheep.image is not None:
                    self.c.delete(asheep.image)
                if asheep.progress_on_jump > 0:
                    asheep.image = self.c.create_image(asheep.x, asheep.y, image=self.sheep_image[True], anchor='nw')
                else:
                    asheep.image = self.c.create_image(asheep.x, asheep.y, image=self.sheep_image[asheep.stretch_leg], anchor='nw')

                    # Flip animation
                    asheep.stretch_leg = not asheep.stretch_leg

        # Draw a counter on left upper
        self.c.itemconfigure(self.id_counter, text=(self.counter_format % self.sheep_count))

    def callback_button_1(self, event):
        self.gate_is_widely_open = True

    def callback_button_release_1(self, event):
        self.gate_is_widely_open = False


def main():
    frame = SheepFrame(Tk())
    frame.start()
    frame.mainloop()

if __name__ == '__main__':
    main()
