# Setup to run

Install following packages

 - Python 2.7 or 3.0 or newer version

# Run the sheep

Launch `sheep.py` on terminal or command prompt

    % python3 sheep.py

# License

WTFPL
